appid = '21377801c3c16aac1850838e6545d2b3'
import requests
import psycopg2
import psycopg2.extras
from datetime import datetime, timedelta

connect = psycopg2.connect(dbname="all_test",
                           user="postgres",
                           password="postgres",
                           host = "localhost",
                           port = 5432)
cursor = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

# Запрос текущей погоды
def request_current_weather(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                     params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        cur_temp = data['main']['temp']
        return cur_temp
    except Exception as e:
        print(e)
        pass

# Прогноз
def request_forecast(city_id):
    ins = """
        INSERT INTO "test_weather" (date, temp, cur_date)
        VALUES (%(date)s, %(temp)s,%(cur_date)s)
    """
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                           params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        d = ()
        cur_date = datetime.now()
        for i in data['list']:
            date = (i['dt_txt'])[:16]
            temp = float(i['main']['temp'])
            di = {"date":date, "temp" : temp, "cur_date":cur_date}
            d = d + (di,)
        cursor.executemany(ins, d)
        connect.commit()
    except Exception as e:
        print(e)
        pass


def get_weather(date_time, days=0):
    y = datetime.strftime(date_time, "%Y")
    m = datetime.strftime(date_time, "%m")
    d = datetime.strftime(date_time, "%d")
    h = date_time.hour
    interes_date = date_time - timedelta(days= days)
    select = """
        SELECT date, temp, MAX(cur_date)
        FROM "test_weather"
        WHERE date_part('hour', date) >= %s AND date_part('hour', date) <= %s
            AND cur_date <= %s
            AND date_part('year', date) = %s
            AND date_part('month', date) = %s
            AND date_part('day', date) = %s
        GROUP BY date, temp
        ORDER BY date DESC
    """
    cursor.execute(select, (h-3,h+3, interes_date,y,m,d))
    info = cursor.fetchall()
    i = 0
    if len(info) == 0:
        return False
    while i != len(info)-1:
        if info[i]["date"] == info[i+1]["date"]:
            del info[i]
        else:
            i += 1
    return info

city_id = 524901

request_forecast(city_id)
temp_now = request_current_weather(city_id)
a = 0
info = get_weather(datetime.now(), a)
if info:
    for e in info:

        percent = abs(temp_now - e["temp"])/temp_now*100
        print ("Температура сейчас: ", temp_now )
        print ("Предсказана {} для дней(ня) назад для {} часов: ".format(a, datetime.now().hour), e["temp"])
        print ("Отличие на {} % ".format(percent))
